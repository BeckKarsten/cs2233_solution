List = []
string = str(input())
x = input('enter something into the array, if finished enter "end": ')

while x != 'end':
    List.append(x)
    x = input('enter something into the array, if finished enter "end": ')


def first(list_two: list, string_2: str):
    string_2 = string_2.lower()
    if string_2 == 'end':
        return list_two[-1]
    elif string_2 == 'start' or 'first':
        return list_two[0]


x = first(List, string)
print(x)

pass
